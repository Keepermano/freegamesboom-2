# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr ""

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "เกมฟรีบูม"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "ค้นหาเกมมากกว่า 100,000 เกม"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "ค้นหา"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "ใหม่"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "เป็นที่นิยม"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "เกมที่ชื่นชอบ"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "เล่นครั้งสุดท้าย"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "ข้อมูล"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "ประเภท"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr ""

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "เล่น"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "เกมนี้ไม่สามารถเล่นได้บนมือถือ"

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "การควบคุม"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "วีดีโอ"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "เล่น!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "หมวดหมู่ยอดนิยม"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "คะแนนโหวต"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "ไม่ทำงานใช่ไหม"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "ที่เพิ่ม"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "เพิ่มในรายการโปรด"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "หุ้น"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "เต็มจอ"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "ล้างรายการ"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "จัดเรียงตาม"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "ที่นิยมมากที่สุด"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "แท็ก"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "แท็กยอดนิยม"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "เกมที่นิยมมากที่สุด"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "แท็กยอดนิยม"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "ที่นิยมมากที่สุด"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "แนะนำ FGB!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "มากกว่า %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "เล่นครั้งล่าสุด"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "เกมใหม่"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "เกมใหม่ - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "เกมยอดนิยม - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "เกมยอดนิยม"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "เกมยอดนิยม"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "เกมที่ชื่นชอบ"

#: catalog/views.py:194
msgid "Last Played"
msgstr "เล่นครั้งสุดท้าย"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "หน้าแบน"

#: flatpages/models.py:12
msgid "Title"
msgstr "หัวข้อ"

#: flatpages/models.py:14
msgid "Text"
msgstr "ข้อความ"

#: flatpages/models.py:15
msgid "Active"
msgstr "คล่องแคล่ว"

#: flatpages/models.py:16
msgid "Modified"
msgstr "ดัดแปลง"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "หน้าคงที่"

#: flatpages/models.py:20
msgid "Static page"
msgstr "หน้าคงที่"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "แอฟริกาใต้"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "ภาษาอาหรับ"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "อาเซอร์ไบจัน"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "บัลแกเรีย"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "เบลารุส"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "ประเทศบังคลาเทศ"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "บอสเนีย"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "คาตาลัน"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "สาธารณรัฐเช็ก"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "เวลส์"

#: freegamesboom/settings.py:165
#, fuzzy
#| msgid "Spanish"
msgid "Danish"
msgstr "สเปน"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "เยอรมัน"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "กรีก"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "อังกฤษ"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "ภาษาโลก"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "สเปน"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "เอสโตเนีย"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "ฟินแลนด์"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "ฝรั่งเศส"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "ไอริช"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "กาลิเซีย"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "ชาวอิสราเอล"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "ภาษาฮินดี"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "โครเอเชีย"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "ฮังการี"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "อาร์เมเนีย"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "อิตาลี"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "ญี่ปุ่น"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "จอร์เจีย"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "คาซัคสถาน"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "ขอม"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "ดา"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "เกาหลี"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "ลักเซมเบิร์ก"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "ภาษาลิธัวเนีย"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "ลัตเวีย"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "มาซิโดเนีย"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "มาลายาลัม"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "มองโกเลีย"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "ฐี"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "พม่า"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "เนปาล"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "ดัตช์"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "เซติ"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "ปัญจาบ"

#: freegamesboom/settings.py:202
#, fuzzy
#| msgid "English"
msgid "Polish"
msgstr "อังกฤษ"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "โปรตุเกส"

#: freegamesboom/settings.py:204
#, fuzzy
#| msgid "Armenian"
msgid "Romanian"
msgstr "อาร์เมเนีย"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "รัสเซีย"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "สโลวาเกีย"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "ภาษาสโลเวเนีย"

#: freegamesboom/settings.py:208
#, fuzzy
#| msgid "Armenian"
msgid "Albanian"
msgstr "อาร์เมเนีย"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "เซอร์เบีย"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "สวีเดน"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "ภาษาสวาฮิลี"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "มิลักขะ"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "กู"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "ไทย"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "ตุรกี"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "ตาตาร์"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "ยูเครน"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "ภาษาอูรดู"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "เวียตนาม"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr ""

#: templates/404.html:18
msgid "404 Error"
msgstr "ข้อผิดพลาด 404"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "อุ่ย หน้านี้ไม่มีอยู่"

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "มากกว่า"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "ทั้งหมด"

#~ msgid "Indonesian"
#~ msgstr "ชาวอินโดนีเซีย"

#~ msgid "Kabyle"
#~ msgstr "เบิล"

#~ msgid "Udmurt"
#~ msgstr "อุดมูร์ต"
