# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr ""

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr ""

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr ""

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr ""

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr ""

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr ""

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr ""

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr ""

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr ""

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr ""

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr ""

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr ""

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr ""

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr ""

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr ""

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr ""

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr ""

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr ""

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr ""

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr ""

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr ""

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr ""

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr ""

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr ""

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr ""

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr ""

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr ""

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr ""

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr ""

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr ""

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr ""

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr ""

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr ""

#: catalog/views.py:75
msgid "Popular games"
msgstr ""

#: catalog/views.py:99
#, python-format
msgid "Similar %s games"
msgstr ""

#: catalog/views.py:191
msgid "Favourite games"
msgstr ""

#: catalog/views.py:194
msgid "Last Played"
msgstr ""

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr ""

#: flatpages/models.py:12
msgid "Title"
msgstr ""

#: flatpages/models.py:14
msgid "Text"
msgstr ""

#: flatpages/models.py:15
msgid "Active"
msgstr ""

#: flatpages/models.py:16
msgid "Modified"
msgstr ""

#: flatpages/models.py:19
msgid "Static pages"
msgstr ""

#: flatpages/models.py:20
msgid "Static page"
msgstr ""

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr ""

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr ""

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr ""

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr ""

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr ""

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr ""

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr ""

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr ""

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr ""

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr ""

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr ""

#: freegamesboom/settings.py:166
msgid "German"
msgstr ""

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr ""

#: freegamesboom/settings.py:168
msgid "English"
msgstr ""

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr ""

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr ""

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr ""

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr ""

#: freegamesboom/settings.py:173
msgid "French"
msgstr ""

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr ""

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr ""

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr ""

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr ""

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr ""

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr ""

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr ""

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr ""

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr ""

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr ""

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr ""

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr ""

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr ""

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr ""

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr ""

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr ""

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr ""

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr ""

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr ""

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr ""

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr ""

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr ""

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr ""

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr ""

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr ""

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr ""

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr ""

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr ""

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr ""

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr ""

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr ""

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr ""

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr ""

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr ""

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr ""

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr ""

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr ""

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr ""

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr ""

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr ""

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr ""

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr ""

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr ""

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr ""

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr ""

#: templates/404.html:18
msgid "404 Error"
msgstr ""

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr ""

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr ""

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr ""
