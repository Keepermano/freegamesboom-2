# -*- coding: utf-8 -*-
import re

from django.core.management.base import BaseCommand
from tqdm import tqdm

from catalog.models import Category, Tag

site = 'http://www.agame.com'


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in tqdm(Category.objects.all()):
            o.tags_new.clear()
            for idx, tag in enumerate(o.tags.all().order_by('target_tag')):
                o.tags_new.add(tag, through_defaults={'order': idx + 1})

        for o in tqdm(Tag.objects.all()):
            o.tags_new.clear()
            for idx, tag in enumerate(o.tags.all().order_by('target')):
                o.tags_new.add(tag, through_defaults={'order': idx + 1})
